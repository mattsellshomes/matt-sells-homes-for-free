I’ve built my business on three things: excellent customer service; innovative, out-of-the-box thinking; and up-to-date market knowledge. I have a proven track record of selling homes faster, for a higher price, than most other agents. Call (214) 850-0174 more information!
||
Address: 2321 Blackstone Dr, Frisco, TX 75033, USA || 
Phone: 214-850-0174
